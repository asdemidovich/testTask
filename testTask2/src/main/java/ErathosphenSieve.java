import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ErathosphenSieve {
    boolean[] primes;

    public ErathosphenSieve() {
    }

    public ErathosphenSieve(int n) {
        primes = new boolean[n];
        Arrays.fill(primes, true);
        for (int p = 2; p < n; p++) {
            if (primes[p]) {
                System.out.println(p);
                for (int j = p; j * p < n; j++) { //p^2<n
                    primes[j * p] = false; //каждое число умноженное на p вычеркивается
                }
            }
        }

    }

    public List<Integer> getSieve() {
        List<Integer> result= new ArrayList<Integer>();
        for (int i =2; i<primes.length; i++){
            if (primes[i]){
                result.add(i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        try {
            int n = Integer.parseInt(args[0]);
            new ErathosphenSieve(n);
        } catch (NumberFormatException e) {
            System.out.println(args[0] + "Not a valid number");
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Wrong number of arguments" );
        }
    }
}
