
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;


public class ErathosphenSieveTest extends Assert {
    @Test
    public void test20(){
        Object[] result = new ErathosphenSieve(20).getSieve().toArray();
        Integer[] expected = {2, 3, 5, 7,11, 13, 17, 19};
        assertArrayEquals(expected , result);
     }
}
