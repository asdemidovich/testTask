
public class MinFibonachiFinder {

    private long previous = 0;
    private long current = 1;

    private long calculateNext() {
        long buff = current;
        current += previous;
        previous = buff;
        return current;
    }

    public long findMin(long n) {
        long result = 0;
        while (result < n) {
            result = calculateNext();
        }
        return result;
    }

    public static void main(String[] args) {
        try {
            long n = Long.parseLong(args[0]);
            MinFibonachiFinder finder = new MinFibonachiFinder();
            System.out.println(finder.findMin(n));
        } catch (NumberFormatException e) {
            System.out.println(args[0] + "Not a valid number");
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Wrong number of arguments" );
        }
    }
}
